//////////////////////////////////////////////////////////
//							
//	UDIM Tool for Softimage				
//	Version: 1.0
//	Language: Javascript
//	Type: PPG Tool					
//	Author: AlbertoGz
//	Website: http://albertogz.com
//							
//	Special thanks to Cesar Saez for help me with this tool.
//					
//////////////////////////////////////////////////////////
//
//	Main functions:
//      -Translate Left/Right and Up/Down in integer steps.
//	    -Rotate CC or CCW in gived angle.
//      -Scale double or half size (x2||/2).
//	    -Flip/flop function in mirror mode.
//
//	Installation:
//  	Simply open .js file in XSI Script editor,
//   	set javascript languaje and run script.
//	    You can drag the script to a button in a
//   	custom toolbar/shelf.
//
//////////////////////////////////////////////////////////


var oPSet = ActiveSceneRoot.AddProperty( "CustomProperty", false, "UDIMtools" ) ;
oPSet.AddParameter3( "incremento", siInt4, 1, 1, 100, false, false) ;
oPSet.AddParameter3( "rotacion", siInt4, 90, 1, 360, false, false) ;
 
var oLayout = oPSet.PPGLayout;
 
//GRUPO TRANSLATE
oLayout.AddGroup("Translate UVW");
    oLayout.AddRow();
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.AddButton( "UP", "  UP  ");
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.EndRow();
 
    oLayout.AddRow();
    oLayout.AddButton( "LEFT" );
 
    oLayout.AddGroup("", false, 2); oLayout.EndGroup(); //espaciador
    oLayout.AddItem( "incremento", " " );
    oLayout.AddGroup("", false, 2); oLayout.EndGroup(); //espaciador
 
    oLayout.AddButton( "RIGHT" );
    oLayout.EndRow();
 
    oLayout.AddRow();
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.AddButton( "DOWN" ) ;
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.EndRow() ;
oLayout.EndGroup();
 
oLayout.AddSpacer();
 
//GRUPO SCALE
oLayout.AddGroup("Scale UVW");
    oLayout.AddRow();
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.AddButton( "Double", "x2");
    oLayout.AddButton( "Half", "/2");
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.EndRow();
oLayout.EndGroup();
 
oLayout.AddSpacer();
 
//GRUPO ROTATE
oLayout.AddGroup("Rotate UVW");
    oLayout.AddRow();
    oLayout.AddGroup("", false, 5); oLayout.EndGroup(); //espaciador
    oLayout.AddItem( "rotacion", " " );
    oLayout.AddButton( "RotateCC", "CC");
    oLayout.AddButton( "RotateCCW", "CCW");
    oLayout.AddGroup("", false, 10); oLayout.EndGroup(); //espaciador
    oLayout.EndRow();
oLayout.EndGroup();
 
oLayout.AddSpacer();
 
//GRUPO FLIP
oLayout.AddGroup("Flip UVW");
    oLayout.AddRow();
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.AddButton( "FlipH");
    oLayout.AddButton( "FlipV");
    oLayout.AddGroup("", false, 30); oLayout.EndGroup(); //espaciador
    oLayout.EndRow();
oLayout.EndGroup();
 
 
oLayout.Logic = UP_OnClicked.toString() +
                DOWN_OnClicked.toString() +
                LEFT_OnClicked.toString() +
                RIGHT_OnClicked.toString() +
                Double_OnClicked.toString() +
                Half_OnClicked.toString() +
                RotateCC_OnClicked.toString() +
                RotateCCW_OnClicked.toString() +
                FlipH_OnClicked.toString() +
                FlipV_OnClicked.toString() +
                OnClosed.toString() +
                get_view.toString();
oLayout.Language = "JScript" ;
show(oPSet, 200, 340);
 
//funciones TRANSLATE
function UP_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    var incremento = PPG.Inspected(0).Parameters("incremento").Value;
    TranslateUVW(v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 0, incremento, 0, siRelative , siUVW);
}
 
function DOWN_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    var incremento = PPG.Inspected(0).Parameters("incremento").Value;
    TranslateUVW (v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 0, -incremento, 0, siRelative , siUVW);
}
 
function LEFT_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    var incremento = PPG.Inspected(0).Parameters("incremento").Value;
    TranslateUVW (v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), -incremento, 0, 0, siRelative , siUVW);
}
 
function RIGHT_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    var incremento = PPG.Inspected(0).Parameters("incremento").Value;
    TranslateUVW (v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), incremento, 0, 0, siRelative , siUVW);
}
 
//funciones SCALE
function Double_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    ScaleUVW(v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 2, 2, 2, siRelative, siUVW, false, null, null, null);
}
 
function Half_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    ScaleUVW(v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 0.5, 0.5, 0.5, siRelative, siUVW, false, null, null, null);
}
 
 
//funciones ROTATE
function RotateCC_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    var rotacion = PPG.Inspected(0).Parameters("rotacion").Value;
    RotateUVW (v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 0, 0, rotacion, siRelative , siUVW, null, null, null, null, null);
}
function RotateCCW_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    var rotacion = PPG.Inspected(0).Parameters("rotacion").Value;
    RotateUVW (v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 0, 0, -rotacion, siRelative , siUVW, null, null, null, null, null);
}
 
//funciones FLIP
function FlipH_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    ScaleUVW(v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), -1, 1, 1, siRelative, siUVW, false, null, null, null);
}
 
function FlipV_OnClicked()
{
    var v = get_view();
    if (!v) return false;
    ScaleUVW(v.GetAttributeValue("selectedsamples"), v.GetAttributeValue("selectedprojections"), 1, -1, 1, siRelative, siUVW, false, null, null, null);
}
 
//CLOSE PPG
function OnClosed()
{
    DeleteObj( PPG.Inspected(0) ) ;
    PPG.Close() ;
}
 
function get_view()
{
    var views = Application.Desktop.ActiveLayout.Views;
    var v = views.Find("Texture Editor");
    if (!v) var v = views.Find("View Manager").Views.Find("Texture Editor");
    if (v) return v;
    return null;
}
 
function show(pset, size_x, size_y)
{
    var v = Desktop.ActiveLayout.CreateView("Property Panel", pset.Name);
    v.BeginEdit();
    v.Resize(size_x, size_y);
    v.SetAttributeValue( "targetcontent", pset.FullName);
    v.EndEdit();
}

//EOF