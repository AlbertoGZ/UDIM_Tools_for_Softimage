UDIM Tools for Softimage

UDIM Tools is a little script to make easier simple and repetitive actions of UV islands, thinking to be useful tool for prepare UDIM sets in Softimage XSI.

Version: 1.0			
Language: Javascript
Type: PPG Tool				
Author: AlbertoGz
Website: http://albertogz.com

Special thanks to Cesar Saez for help me with this tool.

Main functions:
-Translate Left/Right and Up/Down in integer steps.
-Rotate CC or CCW in gived angle.
-Scale double or half size (x2||/2).
-Flip/flop function in mirror mode.

Installation:
Simply open .js file in XSI Script editor, set javascript languaje and run script.
You can drag the script to a button in a custom toolbar/shelf.		